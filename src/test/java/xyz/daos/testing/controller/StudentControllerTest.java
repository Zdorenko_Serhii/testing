package xyz.daos.testing.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import xyz.daos.testing.model.Student;
import xyz.daos.testing.service.StudentService;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class StudentControllerTest {
    private final StudentService studentService = Mockito.mock(StudentService.class);
    private final StudentController sut = new StudentController(studentService);
    private final MockMvc mockMvc = standaloneSetup(sut).build();
    private List<Student> students =
            List.of(new Student(100L, "ForTest"), new Student(101L, "ForTest2"));

    @Test
    public void findByIdTest() throws Exception {
        when(studentService.findById(100L)).thenReturn(students.get(0));
        MvcResult mvcResult = mockMvc.perform(get("/students/100")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String answer = mvcResult.getResponse().getContentAsString();
        assertEquals(new ObjectMapper().readValue(answer, Student.class), students.get(0));
    }

    @Test
    public void findAllUsersTest() throws Exception {
        when(studentService.findAll()).thenReturn(students);
        mockMvc.perform(get("/students")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(students.get(0).getId()), Long.class))
                .andExpect(jsonPath("$[1].id", is(students.get(1).getId()), Long.class))
                .andExpect(jsonPath("$[0].name", is(students.get(0).getName())))
                .andExpect(jsonPath("$[1].name", is(students.get(1).getName())));
    }

    @Test
    public void saveTest() throws Exception {
        doNothing().when(studentService).save(any(Student.class));
        mockMvc.perform(post("/students")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\": \"TEST\"}"))
                //          .andDo(print())
                .andExpect(status().isCreated());
    }
}
