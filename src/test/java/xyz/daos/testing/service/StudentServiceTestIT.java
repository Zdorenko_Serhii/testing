package xyz.daos.testing.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import xyz.daos.testing.AbstractIntegrationTest;
import xyz.daos.testing.exception.StudentNotFoundException;
import xyz.daos.testing.model.Student;
import xyz.daos.testing.repository.StudentRepository;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class StudentServiceTestIT extends AbstractIntegrationTest {
    @Autowired
    private StudentService sut;

    @Autowired
    private StudentRepository repository;

    @Test
    public void shouldFindAllStudents() {
        //GIVEN
        int sizeBefore = sut.findAll().size();
        List<Student> studentsToSave = List.of(
                new Student("1"),
                new Student("2"),
                new Student("3"));
        repository.saveAll(studentsToSave);
        //WHEN
        List<Student> students = sut.findAll();
        //THEN
        assertEquals(sizeBefore, students.size() - studentsToSave.size());
    }

    @Test
    public void shouldFindStudentById() {
        //GIVEN
        Student student = repository.save(new Student("TestStudent"));
        //WHEN
        Student expectedStudent = sut.findById(student.getId());
        //THEN
        assertNotNull(expectedStudent);
        assertEquals(expectedStudent.getId(), student.getId());
    }

    @Test
    public void shouldThrowStudentNotFoundException() {
        assertThrows(StudentNotFoundException.class, () -> sut.findById(-1L));
    }

}
