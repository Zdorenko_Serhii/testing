package xyz.daos.testing.service;

import org.junit.jupiter.api.Test;
import xyz.daos.testing.model.Student;
import xyz.daos.testing.repository.StudentRepository;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class StudentServiceTest {
    private final StudentRepository studentRepository = mock(StudentRepository.class);
    private final StudentService sut = new StudentServiceImpl(studentRepository);
    private List<Student> students =
            List.of(new Student(100L, "ForTest"), new Student(101L, "ForTest2"));

    @Test
    public void shouldFindStudentById() {
        //GIVEN
        when(studentRepository.findById(any())).thenReturn(Optional.of(students.get(0)));
        //WHEN
        Student student = sut.findById(100L);
        //THEN
        verify(studentRepository, times(1)).findById(any());
        assertEquals(students.get(0).getName(), student.getName());
    }

    @Test
    public void shouldFindAllStudents() {
        //GIVEN
        when(studentRepository.findAll()).thenReturn(students);
        //WHEN
        int result = sut.findAll().size();
        //THEN
        assertEquals(students.size(), result);
    }

    @Test
    public void shouldSaveStudent() {
        //GIVEN
        when(studentRepository.findAll()).thenReturn(students);
        //WHEN
        studentRepository.save(students.get(0));
        //THEN
        List<Student> result = (List<Student>) studentRepository.findAll();

        assertTrue(result.contains(students.get(0)));
    }
}
