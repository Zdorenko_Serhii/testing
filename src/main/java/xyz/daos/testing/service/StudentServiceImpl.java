package xyz.daos.testing.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import xyz.daos.testing.exception.StudentNotFoundException;
import xyz.daos.testing.model.Student;
import xyz.daos.testing.repository.StudentRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository repository;

    @Override
    public List<Student> findAll() {
        return (List<Student>) repository.findAll();
    }

    @Override
    public Student findById(Long id) {
        return repository.findById(id).orElseThrow(
                () -> new StudentNotFoundException("Student does not exists with such id: " + id)
        );

    }

    @Override
    public void save(Student student) {
        repository.save(student);
    }
}
