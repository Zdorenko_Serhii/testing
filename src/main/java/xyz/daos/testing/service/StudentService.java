package xyz.daos.testing.service;

import xyz.daos.testing.model.Student;

import java.util.List;

public interface StudentService {
    List<Student> findAll();

    Student findById(Long id);

    void save(Student student);
}
