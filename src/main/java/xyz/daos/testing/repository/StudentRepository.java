package xyz.daos.testing.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import xyz.daos.testing.model.Student;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {
}
