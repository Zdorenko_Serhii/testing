package xyz.daos.testing.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import xyz.daos.testing.exception.StudentNotFoundException;
import xyz.daos.testing.model.Student;
import xyz.daos.testing.service.StudentService;

import java.util.List;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentController {
    private final StudentService studentService;

    @GetMapping("/{studentId}")
    public Student findById(@PathVariable("studentId") Long id) {
        try {
            return studentService.findById(id);
        }catch (StudentNotFoundException ex){
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, "Student Not Found", ex);
        }
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public List<Student> findAll() {
        return studentService.findAll();
    }

    @PostMapping
    public HttpEntity<Void> save(@RequestBody Student student) {
        studentService.save(student);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }
}
