package xyz.daos.testing.model;

import lombok.*;
import org.springframework.lang.NonNull;

import javax.persistence.*;

@Data
@Entity
@Table(name = "students")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NonNull
    @Column(name = "name")
    private String name;

    public Student(Long id, @NonNull String name) {
        this.id = id;
        this.name = name;
    }

    public Student() {
    }

    public Student(@NonNull String name) {
        this.id = id;
        this.name = name;
    }
    protected boolean canEqual(final Object other) {
        return other instanceof Student;
    }

}
